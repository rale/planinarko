({
    baseUrl: "js/lib",
    map: { '*': { 'jquery': 'zepto' } },
        paths: {
        zepto: 'lib/zepto',
        underscore: 'lib/underscore',
        text: 'lib/text',
        backbone: 'lib/backbone',
        utils: 'lib/utils'
    },
    dir: "../www-built",
    appDir: "../www",
    removeCombined: true,
    modules: [
        { name: "../app" }
    ]
})