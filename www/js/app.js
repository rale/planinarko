define(['backbone', 'zepto', 'underscore',  'router'], function(Backbone, $, _, Router){
    
    var initialize = function(){ 


    	Router.initialize(); 

    	vent = _.extend({}, Backbone.Events);
 
		vent.on("some:event", function(){
		    
		    window.location.reload();
		});
		 

		Backbone.View.prototype.close = function(){
			if (this.onClose){
				this.onClose();
			}
			this.remove();
			this.undelegateEvents();
			this.unbind();
		};

	}

	return { 
    	initialize: initialize 
	}

});