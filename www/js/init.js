require.config({
	
    paths: {
        zepto: 'lib/zepto',
        underscore: 'lib/underscore',
        text: 'lib/text',
        backbone: 'lib/backbone',
        backboneCache: 'lib/backbone.fetch-cache',
        utils: 'lib/utils'
    },
    shim: {
        backbone: {
            deps: ['zepto', 'underscore'],
            exports: 'Backbone'
        },
        underscore: {
            exports: '_'
        }
    }
});

require(['app'], function(App) {

    ACTIVE_LANG = localStorage.getItem('activeLanguage') || "EN";

    $.ajax({
        url: "/data/translations/" + ACTIVE_LANG + "-data.json",  
        dataType: 'json',   
        'success': function(data){
            translate = data;
        }
    });

    App.initialize();

});

