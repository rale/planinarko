define(['zepto'], function ($) {

	var utils = {

		setHeader: function(xhr){
			xhr.setRequestHeader('X-Parse-Application-Id', 'Vu1wr0xBJgVS0viA7hF6F6eCYCnbFtDeV19KHLWR');
            xhr.setRequestHeader('X-Parse-REST-API-Key', 'CsEbtv7nn8sFJp7N43uPCkum8AfOcKRj4HV4GsAk');
		},

		showView: function(view){
			if(view){

				if (this.currentView){
					this.currentView.close();
				}
				this.currentView = view;
				this.currentView.render();
				$('#content').append(this.currentView.el);
		 	}
		}
	}
	
	return utils;
}) 