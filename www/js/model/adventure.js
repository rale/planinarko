
define([], function() {

        var adventure = Backbone.Model.extend({
             urlRoot: "https://api.parse.com/1/classes/Location/",
             defaults: {  "createdAt" : "2013-07-21T11:28:53.524Z",
                          "description_RS" : "VVVRtanj, mistična srpska planina je poznat svakom planinaru sa ovih prostora. Za mnoge je uspon na Rtanj već postao kultni doživljaj i jedan od bitnijih datuma u planinarskim kalendarima. Postoje tradicionalne akcije kao štu su Božićni ili noćni uspon, ali postoje i mnoge druge akcije koje planinari i drugi ljubitelji ove planine izvode što samostalno što u grupama. Interesantno je da je prilaz Rtnju moguć praktično sa svih strana, pa se u zavisnosti od trenutnih interesovanja i mogućnosti, bira željeni put. Ovim tekstom će biti opisan jedan od težih načina da se popne na Rtanj, ali verovatno i jedan od najlepših.",
                          "highestPoint" : 1570,
                          "latitude" : 43.776288999999998,
                          "longitude" : 21.876698000000001,
                          "lowestPoint" : 310,
                          "distance" : '0',
                          "name_RS" : "Rtanj: Lukovo-Šiljak-Mirovo ",
                          "objectId" : "pSSf6u2xVc",
                          "routeLength" : 15000,
                          "shapeLevel" : 7,
                          "tehnicalLevel" : 5,
                          "totalMetersClimbed" : 1270,
                          "updatedAt" : "2013-07-24T20:56:05.964Z"
                        },
              setAdventure: function(adventureId) {
                  this.urlRoot = "https://api.parse.com/1/classes/Location/" + adventureId;
              }
        });

        return adventure;

});