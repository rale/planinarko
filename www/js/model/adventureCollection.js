
define(['model/adventure'], function(Adventure) {

        var adventureCollection = Backbone.Collection.extend({
             model: Adventure,
             url: "https://api.parse.com/1/classes/Location",
             fetchNamesOnly:function () {
                this.url = 'https://api.parse.com/1/classes/Location?keys=name_' + ACTIVE_LANG + ',shapeLevel,tehnicalLevel,highestPoint,peek,longitude,latitude';
             },
             fetchFullObjects:function () {
                this.url = 'https://api.parse.com/1/classes/Location'
            },
            _sortBy: "name_RS",
            sortLocations: function(sort_by) {
                this._sortBy = sort_by;
                this.sort();
            },
            comparator: function(adventure){
                return adventure.get(this._sortBy);
            },

            getCurrenLocation: function() { 
                
                var that = this;
                var geoService = navigator.geolocation;
                
                var coordinates = function(position){
                    
                    var lat = position.coords.latitude;
                    var lon = position.coords.longitude;
                    
                    _.each(that.models, function(model){
                        
                        var lat1 = lat;
                        var lon1 = lon;

                        var lon2 = model.get('longitude');
                        var lat2 = model.get('latitude');

                        var R = 6371; 
                        var dLat = (lat2-lat1) * Math.PI / 180;
                        var dLon = (lon2-lon1) * Math.PI / 180;
                        lat1 = lat1 * Math.PI / 180;
                        lat2 = lat2 * Math.PI / 180; 

                        var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                                Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
                        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
                        var d = R * c;

                        model.set("distance", d);

                    });
                };

                if(geoService){
                        geoService.getCurrentPosition(coordinates, this.errorHandler);
                } else {
                    alert('Da bi ste koristili ovu opciju morate ukluciti Geolokaciju');
                }
            },

            setCurrentLocation: function(position){
                this.currentLatitude = position.coords.latitude;
                this.currentLongitude = position.coords.longitude;
            },

            errorHandler: function(){
                alert("Doslo do greske, molimo Vas da proverite da li je uklucena geolokacija");
            }
            
        });

        return adventureCollection;

});