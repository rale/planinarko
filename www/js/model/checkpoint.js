define(['underscore', 'backbone'], function(_, Backbone) {

	var checkpoint = Backbone.Model.extend({
		urlRoot: "https://api.parse.com/1/classes/Checkpoint/",
		defaults: {
                  "height": 405,
                  "latitude": 45.157064,
                  "location_id": "HNeRWicrI4",
                  "longitude": 19.728317,
                  "name_RS": "Stena",
                  "order": 7,
                  "createdAt": "2013-07-24T21:07:00.292Z",
                  "updatedAt": "2013-08-04T11:57:40.052Z",
                  "objectId": "S61Pw54saf"
            }
	});

	return checkpoint;
});