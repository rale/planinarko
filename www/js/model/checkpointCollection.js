define(['underscore', 'backbone', 'model/checkpoint'], function(_, Backbone, Checkpoint) {

  var checkpointCollection = Backbone.Collection.extend({
      model: Checkpoint,
      url: 'https://api.parse.com/1/classes/Checkpoint',
      setAdventure:function(locationId) {
        this.url = 'https://api.parse.com/1/classes/Checkpoint/?order=order&where={"location_id":"' + locationId + '"}'; 
      },
      defaults: { "results": [
       {
            "height": 405,
            "latitude": 45.157064,
            "location_id": "HNeRWicrI4",
            "longitude": 19.728317,
            "name_RS": "Stena",
            "order": 7,
            "createdAt": "2013-07-24T21:07:00.292Z",
            "updatedAt": "2013-08-04T11:57:40.052Z",
            "objectId": "S61Pw54saf"
        },
        {
            "height": 487,
            "latitude": 45.157994,
            "location_id": "HNeRWicrI4",
            "longitude": 19.741531,
            "name_RS": "Brankovac",
            "order": 8,
            "createdAt": "2013-07-24T21:07:00.904Z",
            "updatedAt": "2013-08-04T11:57:40.947Z",
            "objectId": "RVTGhvCwls"
        }
      ]
    }
  });
  return checkpointCollection;
});