
define(['underscore', 'backbone'], function( _, Backbone) {

        var club = Backbone.Model.extend({
             urlRoot: "https://api.parse.com/1/classes/Club/",
        });
        return club;
});