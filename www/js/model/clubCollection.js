
define(['underscore', 'backbone', 'model/club'], function(_, Backbone, Club) {

        var clubCollection = Backbone.Collection.extend({
             model: Club,
             url: "https://api.parse.com/1/classes/Club",
        });

        return clubCollection;
});