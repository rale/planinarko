define(['underscore', 'backbone'], function(_, Backbone) {

	var page = Backbone.Model.extend({

            initialize: function(){

            },

            urlRoot: "/data/about.json",

            setUrl: function(pageId){
                  this.urlRoot = "/data/" + pageId + ".json"
            }

      });

	return page;
});