
define(['backbone', 'zepto', 'underscore', 'utils', 'backboneCache'], function (Backbone, $, _, utils) {


  var appRouter = Backbone.Router.extend({

    initialize: function(){
       require(['model/menu', 'views/menu'], function (menuModel, menuView) {
          var menu_model = new menuModel();
          menu_model.fetch({
            'success': function(data){
              var menu_view = new menuView({ el: $('#menu'), model: data });
              menu_view.render();
            }
          });
       });
    },
    
    routes: {
      "": "index",    
      "adventures": "adventures",
      "adventure/:id": "adventure",
      "adventure/:id/map": "map",
      "contact": "contact",
      "clubs":"clubs",
      "page/:id": "page"
    },

    index: function() {
      require(['model/adventureCollection', 'views/index'], function (adventureCollection, indexView) {
        var adventureCollection = new adventureCollection();
        var that = this;
        adventureCollection.fetch({
          beforeSend: utils.setHeader,
          cache: true,
          expires: 86400,
          'success': function(data){
            var index_view = new indexView({el: $('#content div'), collection: data});
            utils.showView(index_view);
           },
          error: function (errorResponse) {
            alert('Greška u radu aplikacije. Molimo da proverite internet konekciju.')
          }
        })
        
      });
    },

    adventures: function() {
      require(["model/adventureCollection", "views/adventures"], function (adventureCollection, adventuresView) {
        var adventure_collection = new adventureCollection();
        adventure_collection.fetchNamesOnly();
        adventure_collection.fetch({
          cache: true,
          expires: 86400,
          beforeSend: utils.setHeader,
          'success': function(){
            var adventures_view = new adventuresView({ el: $('#content div'), collection: adventure_collection });
            utils.showView(adventures_view);
          },
          error: function (errorResponse) {
            alert('Greška u radu aplikacije. Molimo da proverite internet konekciju.')
          }
        });
        
      });
    },

    adventure: function(id) {
      require(['model/adventure', 'views/adventure'], function (adventureModel, adventureView) {
        var adventure_model = new adventureModel();
        adventure_model.setAdventure(id);
        adventure_model.fetch({
          cache: true,
          expires: 86400,
          beforeSend: utils.setHeader,
          'success': function(data){
            var adventure_view = new adventureView({el: $('#content div'), model: data, id: id});
            utils.showView(adventure_view);
          },
          error: function (errorResponse) {
            alert('Greška u radu aplikacije. Molimo da proverite internet konekciju.')
          }
        });
        
      });
    },

    map: function(id) {
      require(['model/checkpointCollection', "views/map"], function (checkpointCollection, mapView) {
        var cehckpoints = new checkpointCollection();
        cehckpoints.setAdventure(id);
        cehckpoints.fetch({
          cache: true,
          expires: 86400,
          beforeSend: utils.setHeader,
          'success': function(data){
            var map_view = new mapView({ el: $('#content div'), id: id, collection: data });
            utils.showView(map_view);
          },
          error: function (errorResponse) {
            alert('Greška u radu aplikacije. Molimo da proverite internet konekciju.')
          }
        });
      });
    },

    contact: function() { 
      require(["views/contact"], function (contactView) {
        var contact_view = new contactView({ el: $('#content div') });    
        utils.showView(contact_view);    
      });
    },

    page: function(id) { 
      require(["views/page", "model/page"], function (pageView, pageModel) {
        var page_model = new pageModel();
        page_model.setUrl(id);
        page_model.fetch({
          cache: true,
          expires: 86400,
          'success': function(data){
            var page_view = new pageView({ el: $('#content div'), model: data });    
            utils.showView(page_view); 
          },
          error: function (errorResponse) {
            alert('Greška u radu aplikacije. Molimo da proverite internet konekciju.')
          }
        })
   
      });
    },

    clubs: function() {
      require(['model/clubCollection',"views/clubs"], function (clubModel, clubView) {
        var club_model = new clubModel();
        club_model.fetch({
          cache: true,
          expires: 86400,
          beforeSend: utils.setHeader,
          'success': function(data){
            var club_view = new clubView({
              collection: data,
              el: $('#content div')
            });
            utils.showView(club_view);            
          },
          error: function (errorResponse) {
            alert('Greška u radu aplikacije. Molimo da proverite internet konekciju.')
          }
        });
      });
    }

  });

  var initialize = function() {
    var app_router = new appRouter();
    Backbone.history.start({ pushState: false });
  };

  return {
    initialize: initialize
  }

});