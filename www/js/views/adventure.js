define(['text!templates/adventure.html'],
 function(adventureTemplate) {

	var adventureView =  Backbone.View.extend({

		initialize: function(adventure_id) {

		},

		render: function(){
			var template = _.template(adventureTemplate, { adventure: this.model });
			this.$el.html(template);		

			setTimeout(function () {
	            $("html,body").animate({ scrollTop: 0 }, "slow");
	        }, 1);

		},

		events: {
			"click #tab-options a.tab-button" : "showData"
		},

		showData: function(e){
			e.preventDefault();

			$('#tab-options a, #tab-options span').removeClass('active');
			$(e.target).addClass('active');
			var data = $(e.target).data('about');

			$('#description p').addClass('hidden');
			$('#description p.' + data).removeClass('hidden');

			$('#description h1 img').removeClass('active');
			$('#icon-' + data).addClass('active');

			return false;
		}

	});

	return adventureView;

});