
define(['text!templates/adventures.html'], function(adventuresTemplate) {
	

	var adventuresView = Backbone.View.extend({
		
		el:$('#content div'),
		
		initialize: function() {
			this.collection.on('sort', this.render, this);
		},

		render: function() {
			var compiledTemplate = _.template( adventuresTemplate, { adventures : this.collection.models });
			this.$el.html(compiledTemplate);
		},

		events:{
			"click #sort" : "showSort",
			"click #footer-links a" : "sort"
		},

		showSort: function(e){
			e.preventDefault();
			$('#footer-links').toggleClass('active');
		},

		sort: function(e){
			e.preventDefault();
			
			var sort_by = $(e.target).data('sort-by');
			if(sort_by !="distance"){
				this.collection.sortLocations(sort_by);
			}else{
				this.collection.getCurrenLocation();
				this.collection.sortLocations(sort_by);
			};
		}
		
	});

	return adventuresView;

});