
define(['text!templates/clubs.html'], function(clubTemplate) {

	var clubView = Backbone.View.extend({
		initialize: function() {
   
		},

		events: {
    		"click #call" : "phoneCall",
    		"click #sms" : "sendSms",
    		"click #email" : "sendEmail",
    		"click .club-container" : "showClubDetails"
		},

		showClubDetails: function(element){
			$(element.target).toggleClass('active');
		},

		phoneCall: function(e){
			var call = new MozActivity({
		      name: "dial",
		      data: {
		      	number: "+381605896222"
		      }
  			});
		},

		sendSms: function(e){
			var newSMS = new MozActivity({
				name: "new",
				data: {
				type : "websms/sms",
					number: "+381605896222"
				}
    		});
		},

		sendEmail: function(e){
			var newSMS = new MozActivity({
				name: "new",
				data: {
					type : "mail"
				}
			});
		},

		render: function() {	
			var compiledTemplate = _.template( clubTemplate, { clubs: this.collection.models });
			this.$el.html(compiledTemplate);
		}
	});

	return clubView;

});