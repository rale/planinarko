define(['text!templates/contact.html'],
 function(contactTemplate) {

	var contactView = Backbone.View.extend({

		initialize: function() {
			
		},

		events: {
    		"click #call" : "phoneCall",
    		"click #sms" : "sendSms",
    		"click #email" : "sendEmail"
		},

		phoneCall: function(e){
			var call = new MozActivity({
		      name: "dial",
		      data: {
		      	number: "+381693263771"
		      }
  			});
		},

		sendSms: function(e){
			var newSMS = new MozActivity({
				name: "new",
				data: {
				type : "websms/sms",
					number: "+381693263771"
				}
    		});
		},

		sendEmail: function(e){
			var newSMS = new MozActivity({
				name: "new",
				data: {
					type : "mail",
					subject: "Planine Srbije - kontakt",
					to: "rastko.milovanovic.81@gmail.com"
				}
			});
		},

		render: function() {
			var template = _.template(contactTemplate, {}); 
			this.$el.html(template);
		}

	});

	return contactView;

});