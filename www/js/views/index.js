define(['text!templates/index.html'], 
	function(indexTemplate) {

	var indexView = Backbone.View.extend({

		initialize: function() {
	        var that = this;			
		},

		random: function() {
			var adventuresCount = this.collection.length,
			randomIndex = (Math.floor((Math.random()*100)+1) % adventuresCount +1) - 1;
			var randomAdventureId = this.collection.at(randomIndex);
			
			return(randomAdventureId.get('objectId'));
		},

		render: function() {
			var randomAdventureId = this.random();
			var template = _.template(indexTemplate, {randomAdventureId:randomAdventureId}); 
			$(this.$el).html(template);	
		}

	});

	return indexView;

});