define(['model/checkpoint', 'model/checkpointCollection', 'text!templates/map.html'],
 function(Checkpoint, CheckpointCollection, mapTemplate) {

	var mapView = Backbone.View.extend({

		initialize: function() {
			var that = this;	
			console.log(this.collection);
		},

		events: {
			"click #locate-me" : "geolocateMe"
		},

		geolocateMe: function(e){
			e.preventDefault();
			var x=document.getElementById("locate-me");

			if (navigator.geolocation){
				navigator.geolocation.getCurrentPosition(showPosition);
			}
			else{
				x.innerHTML="Geolocation is not supported by this browser.";
			}

			function showPosition(position){
				var that = this;

				var marker = new google.maps.Marker({
				  position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
				  map: that.map,
				  title: "Vi ste ovde!",
				  icon: "/img/icons/32x32.png"
				});

				var infowindow = new google.maps.InfoWindow({
      				content: "<strong style='width: 200px'>OVDE</strong>"
				 });

				infowindow.open(map,marker);

				google.maps.event.addListener(marker, 'click', function() {
   				  	infowindow.open(map,marker);	
  				});

  				map.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
			}

		},

		loadMap: function(mapOptions) {
			var that = this,
			mapOptions = {
				zoom: 12,
				center: new google.maps.LatLng(that.collection.at(0).get('latitude'), that.collection.at(0).get('longitude'))
			};
			map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);	

			$.each(this.collection.toJSON(), function(index, val){

				var marker = new google.maps.Marker({
				  position: new google.maps.LatLng(val.latitude, val.longitude),
				  map: map,
				  icon: "/img/icons/32x32.png"
				});
				
				var infowindow = new google.maps.InfoWindow({
      				content: val.name_RS
				 });

				google.maps.event.addListener(marker, 'click', function() {
   				  	infowindow.open(map,marker);	
  				});

			});

			google.maps.event.addDomListener($('#locate-me'), 'click', that.geolocateMe);

		},

		render: function() {
			var that = this,

			template = _.template( mapTemplate, {checkpoints: this.collection} ); 
			
			$(this.el).html(template);

			setTimeout(function () {
	            that.loadMap(that.mapOptions);
	        }, 0);
		}

	});

	return mapView;
});