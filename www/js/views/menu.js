define(['text!templates/menu.html'],
 function(menuTemplate) {

	var menuView =  Backbone.View.extend({

		initialize: function() {

		},

		render: function(){
			var template = _.template(menuTemplate, { menu: this.model });
			this.$el.html(template);
			console.log(ACTIVE_LANG);		
		},

		events: {
			"click #EN" : "changeLanugage",
			"click #RS" : "changeLanugage"
		},

		changeLanugage: function (e){
			e.preventDefault();
			if($(e.target).data('lang') != ACTIVE_LANG){
				localStorage.setItem('activeLanguage', $(e.target).data('lang'));
				vent.trigger("some:event");
			}	
		}

	});

	return menuView;

});