define(['text!templates/page.html'],
 function(pageTemplate) {

	var adventureView =  Backbone.View.extend({

		initialize: function() {

		},

		render: function(){
			var template = _.template(pageTemplate, { page: this.model });
			this.$el.html(template);	
		},

		events: {
			
		}

	});

	return adventureView;

});